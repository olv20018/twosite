
var checkEmail = document.getElementById('checkEmail');
if(checkEmail){ //проверка на существование checkEmail
	checkEmail.addEventListener('blur', sendEmail); // событие "blur" - вызывается когда элемент теряет фокус (при деактивации)

function sendEmail(){
	var allData = new FormData(document.forms.reg); //храним данные в виде массива ключ-значение
	//console.log(allData.getAll('email') );

	var req = new XMLHttpRequest();//позволяет отправлять и получать данные на сервер без перезагрузки страницы
    req.addEventListener('readystatechange' , function(){   //readystatechange - это событие XMLHttpRequest возникает несколько раз когда изменяется состояние объекта req,
              //console.log(req.readyState)
               if(req.readyState==4){ // приходит ответ:1 - когда выполняется функция open // 2 - данные посланы // 3 - ответ начинает приходить// 4 - цикл окончен ответ пришел можно что то делать 
               	if(req.status == 200){ // проверка ответа по статусу  // ответ 200 - все хорошо - страница доступна

                    //console.log(req.responseText);
               		  if(req.responseText)
                    {// responseText - ответ от cервера это то что выводится в php через echo и print_r
                      if(document.querySelector('.successEmail'))
                        document.querySelector('.successEmail').remove();

                      if(!document.querySelector('.errorEmail'))
                      {         
                        checkEmail.insertAdjacentHTML('afterEnd', '<small class="text-danger errorEmail">Пользователь уже существует </small>'); //checkEmail - текстовое поле   // afterEnd - после элемента
                        //Метод insertAdjacentHTML позволяет вставлять произвольный HTML в любое место документа
                            //4 варианта добавления
                           /*`beforeBegin` -- перед `elem`.
2.                          `afterBegin` -- внутрь `elem`, в самое начало.
3.                           `beforeEnd` -- внутрь `elem`, в конец.
4.                            `afterEnd` -- после `elem`.*/
               	      }
                    }

                  else{
                   if(document.querySelector('.errorEmail'))
                      document.querySelector('.errorEmail').remove();

                  if(!document.querySelector('.successEmail'))
                    checkEmail.insertAdjacentHTML('afterEnd', '<small class="text-success successEmail">Пользователь не существует </small>');
                  }
           } 
        }


    });
    req.open("POST", "/user/check", true);  // отработает функция check в userController  // true - асинхронный режим
    req.send(allData); //send - отправляет данные
}

(function ($){
$(function() { 

  var page = 0;  // первая страничка

///////////  16.07.18


$(window).scroll(function(){
        var isBottom = $(window).scrollTop() + $(window).height() + 200 >=  //$(window).scrollTop() - количество прокрученных пикселов + $(window).height - высота окна + 200px
              $(document).height(); //высота документа 
        if(isBottom && !$('body').hasClass('loading'))
        {
            //console.log(123);
            $('body').addClass('loading'); // в body добавляется класс loading - идет загрузка
            page++; //conыole log  получаем 1 // увеличиваем на 1

            $.ajax({   //функция аякс - отправка данных на сервер и получение результата без перезагрузки страницы
                     
                  type: 'POST',  // способ отправки
                  url: '/main/getPage',  //вызываем функцию в main/controller // function getPage()
                  dataType: "json",//какой формат ответа прийдет от сервера чаще используют json
                  data: {  // массив
                        
                        p: page//р-ключ   //передаем данные ввиде объекта
                     
                     },
                  success: function(data){  // событие success возникает, когда данные приходят
                  //console.log(data);  // строка
                  for(var i=0; i<data.length; i++)
                    addBook(data[i]);     // (data[$i]) -данные одной книги
                    $('body').removeClass('loading');  //удаление класса после загрузки картинок
                  }
            });


        }
}); 


   function  addBook(book){
    var html = '<div class="card col-sm-3">';
    html+='<img class="card-img-top" src="http://via.placeholder.com/350x150">';
    html+='<div class="card-body">';
    html+='<h5 class="card-title">' +book.name + '</h5>';
    html+='<p class="card-text">' +book.price + '></p>';
    html+='</div>';
    html+='<ul class="list-group list-group-flush">';
    html+='<li class="list-group-item">' +book.themes + '</li>';
    html+='  <li class="list-group-item">' +book.category + '</li>';
    html+='  <li class="list-group-item">' +book.izd + '</li>';
    html+=' </ul>';
  
html+='</div>';

$(' .books').append(html);
}

});
})(jQuery);

$.ajax({
    type: 'GET',
    url:' https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5',
          dataType: 'json',
          success:function(data){
            console.log(data);
               // var html = 'Курс доллара:' + data[0].sale;
                var html = 'Курс доллара: продажа -' + data[0].sale + '<br> покупка: ' + data[0].buy;


                $('body'). prepend(html);//добавляем в начало курс доллара

          }   

});



 