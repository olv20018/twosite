<?php

class MenuController extends Controller{
          function index(){
            $title = 'Управление меню';
            $menu = new Menu();
            $itemsMenu = $menu->getAll();   //  метод класса меню 
            
            View::render('menu/index', compact('title' , 'itemsMenu'));

          }
      
function add(){
            $title = 'Добавление пункта меню';
            if($_POST){
                $name = isset($_POST['text'])?$_POST['text']:'';//получаем текст
                $path = isset($_POST['path'])?$_POST['path']:'';
                $order_menu = isset($_POST['order_menu'])?$_POST['order_menu']:0; 

            if(!empty($name) && !empty($path)){
            	$menu = new Menu();
            	if( $menu->save($name, $path, $order_menu))
            {

  Session::setMessage('success', "Пункт $name добавлен!");


            	header('Location: /menu');
            	exit();
        }		
    
   }
   else{
    Session::setMessage('danger', "Заполните данные!");
   }


  }
            View::render('menu/add', compact('title'));


               }

 function edit(){//функция редактирования 
  if($_POST){
                $name = isset($_POST['text'])?$_POST['text']:'';//получаем текст
                $path = isset($_POST['path'])?$_POST['path']:'';
                $order_menu = isset($_POST['order_menu'])?$_POST['order_menu']:0;
                $id = isset($_POST['id'])?$_POST['id']:''; 

            if(!empty($name) && !empty($path)){
              $menu = new Menu();
              if( $menu->update/*save*/($name, $path, $order_menu, $id))
            {
 
       Session::setMessage('success', "Пункт $name добавлен!");
       


              header('Location: /menu');
              exit();
            }   
      }

       else{
    Session::setMessage('danger', "Заполните данные!");
   }

}
 	 $title = 'Редактирование';
 	 $id = isset($_GET['id'])?$_GET['id']:'';
 	 if(!empty($id))
 	  {
 	  	$menu = new Menu();  //объект класса меню
 	  	$itemMenu=$menu->edit($id); // метод который получает данные по id
      	View::render('menu/edit', compact('itemMenu', 'title'));  //передача данных
      }		
	else{
		header('Location: /menu');
		exit();
    
	} 
}


 function delete(){
 	$id = isset($_GET['id'])?$_GET['id']:'';
 	            if(!empty($id))
 	  {

 	  	     $menu = new Menu();
 	  	     $menu->delete($id);
 	  }          	
      header('Location: /menu');
            	exit();
            


}

}		




 






?>