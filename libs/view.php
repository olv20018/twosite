<?php
class View{
	static public function render($path, $data=[]){
		extract($data);//ассоциативный массив разбив
		// на переменные где ключи массива становятся
		// названиями переменных  а значения становятся элементами массива
		unset($data);//удаление массива
		require_once 'views/header.php';
		require_once 'views/' . $path .'.php'; //одключение страницы /main/index.php
		require_once 'views/footer.php';
	}
}