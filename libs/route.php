<?php
class Route{
	
	static public function start(){           //статический публичный метод start
	
		$url = isset($_GET['url']) ? $_GET['url'] : 'main';//если гет юрл существует тогда записываем  // 'main' -контроллер который идет по умолчанию
	    $url = explode('/', $url);
	    /*
	    /             $url[0]='main'  //если мы на главной странице то в переменную юрл записывается main  // в массиве в юрл 0 - main
	    /user         $url[0]='user'  //если в адресной строке юзер-юрл 0 -юзер
	    /user/login   $url[0]='user' $url[1]='login'  //  в юрл 0-юзер //  в юрл 1  -  юзер
	    */ 
        //контроллер - это классы
        $nameController = $url[0].'Controller';//'mainController'
        if(file_exists('controllers/'.$nameController.'.php'))    //если в папке контроллер есть этото файл
        {   //   file_exists   -  Проверяет существование указанного файла
            //cotrollers/mainController.php
            require_once 'controllers/'.$nameController.'.php';//связали адресную строку с контролером
            $controller = new $nameController();
            $nameMethod = isset($url[1]) ? $url[1] : 'index';
            if(method_exists($controller, $nameMethod) ) //проверяет существование метода в объекте
            {
            	$controller->$nameMethod();
            }
            else{Route::pageError();}//вызываем статический метод
        }
     else{
     	//echo 'Page Not Found';  // если  впапке нет файла 'mainController'
     	Route::pageError();

     }

	}
	static function pageError(){
		header ('Location: /views/errors/404.html');
	}
}