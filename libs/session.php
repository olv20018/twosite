<?php

class Session{
	static function start(){  //запускает сессию
	     	session_start();
	}
    static function set($key, $value){  // set -записывает 
          	$_SESSION[$key] = $value;  //$_SESSION - ассоциативный массив
	}
	static function has($key){  // has - проверяет сессию с данным ключем
	     		return isset($_SESSION[$key]);
	}
	static function get($key){  // get -  возвращает сессию с дааным ключем
	     		return self::has($key)?$_SESSION[$key]:null;   // если есть сессия с данным ключем мы ее возвращаем иначе null
    }
    static function delete($key){   //удаление сессии 
	        if( self::has($key) ){   //если сессия существует то
	     		unset( $_SESSION[$key] );  // unset - удаляем

			}
    }

    static function setMessage($type, $text){  // setMessage - записывает сообщения 
	        self::set('message', [$type, $text]);  // 'message' - это ключ
     }

    

    static function showMessage(){  //выводим сообщения
	        if (self::has('message') ){   //если есть сообщения
	    echo '<div class="alert alert-'.self::get('message')[0].'">';  //то выводим сообщения 
	    echo '<p>' . self::get('message')[1] . ' </p>';
	    echo '</div>';
	           self::delete('message');  //сообщение вывели и удалили


	        }
	}

}




?>