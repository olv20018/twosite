<?php
class Menu extends Model{   //классс для работы с таблицей меню 
      function getAll(){
        $res = $this->connection->prepare('SELECT * FROM menu ORDER BY order_menu');
        $res->execute();
        return $res->fetchAll(PDO::FETCH_OBJ);

      }

      /* fetchAll() -  возвращает многомерный массив со всеми строками 
      по-умолчанию и нумерованный и ассоциативный массив
      PDO::FETCH_NUM - нумерованный массив;
      PDO::FETCH_ASSOC - ассоциативный массив;
      PDO:: FETCH_OBJ - анонимный объект

       */ 
       
       function save($name, $path, $order_menu){
	$res = $this->connection->prepare('INSERT INTO menu(text, patch, order_menu) VALUES(?,?,?)');
	$res ->execute([$name, $path, $order_menu]);
	return $res; 

}  

  function delete($id)

  {

  	$res = $this->connection->prepare('DELETE FROM menu WHERE id=?');
  	$res ->execute([$id]);





  }

 function edit($id)

  {

  	$res = $this->connection->prepare('SELECT * FROM  menu WHERE id=?');
  	$res ->execute([$id]);
  	 return $res->fetch(PDO::FETCH_OBJ);

  }



 function update($name, $path, $order_menu, $id)

  {

    $res = $this->connection->prepare('UPDATE menu  SET text=?, patch=?, order_menu=? WHERE id=?');

    $res ->execute([$name, $path, $order_menu, $id]);
     return $res;


}
}




?>